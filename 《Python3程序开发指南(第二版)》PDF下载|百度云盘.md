![输入图片说明](121.jpg)

### 内容简介

![输入图片说明](21.png)

Python是一种脚本语言，在各个领域得到了日益广泛的应用。《Python 3程序开发指南（第2版 修订版）》全面深入地对Python语言进行了讲解。

《Python 3程序开发指南（第2版 修订版）》首先讲述了构成Python语言的8个关键要素，之后分章节对其进行了详尽的阐述，包括数据类型、控制结构与函数、模块、文件处理、调试、进程与线程、网络、数据库、正则表达式、GUI程序设计等各个方面，并介绍了其他一些相关主题。全书内容以实例讲解为主线，每章后面附有练习题，便于读者更好地理解和掌握所讲述的内容。

《Python 3程序开发指南（第2版 修订版）》适合于作为Python语言教科书使用，对Python程序设计人员也有一定的参考价值。

### 作者简介

Mark Summerfield，Qtrac公司的所有人，同时还是一位在Python、C 、Qt以及PyQt等领域卓有专长的独立培训专家、顾问、技术编辑与作者。Mark Summerfield撰写的书籍包括《Rapid GUI Programmlng with Python》以及《Qt：The Definitive Guide tO PyQt Programming》 (Addison-Wesley，2008)，并与Jasmin Blanchette共同编写了《C GUI Programming with Qt 4》(Addison-Wesley,2006)。作为Trolltech的文档管理者，Mark创立了并负责编辑Trolltech的技术杂志《Qt Quarterly》。