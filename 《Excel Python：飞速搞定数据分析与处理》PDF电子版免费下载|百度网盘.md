![输入图片说明](121.jpg)

### 内容简介

![输入图片说明](qwert.png)

在如今的时代，大型数据集唾手可得，含有数百万行的数据文件并不罕见。Python是数据分析师和数据科学家的语言。通过本书，即使完全不了解Python，Excel用户也能够学会用Python将烦琐的任务自动化，显著地提高办公效率，并利用Python在数据分析和科学计算方面的突出优势，轻松搞定Excel任务。你将学习如何用pandas替代 Excel函数，以及如何用自动化Python库替代VBA宏和用户定义函数等。

本书既适合Excel用户，也适合Python用户阅读。

### 作者简介

费利克斯·朱姆斯坦（Felix Zumstein），流行开源Python库xlwings的创始人。xlwings帮助Excel用户利用Python脚本将任务自动化，从而实现效率飞跃。费利克斯在工作中接触了大量Excel用户，这使他对Excel在各行各业中的使用瓶颈和解决思路拥有深刻的见解。 【译者简介】 冯黎，Python爱好者，从事桌面应用程序、Web应用程序及游戏的开发工作，曾供职于Singtel。热衷于研究各类编程语言，自Python 2.x时期起开始使用Python，深谙Python的语言特性。

### 编辑推荐

●  **针对零编程基础人士量身打造，适合办公室人群阅读** 

无须丰富的编程经验即可开始使用Python，借助编程的力量，轻松突破Excel的瓶颈，避免人为错误，将Excel和数据库连接并获取数据，让烦琐的Excel任务自动化，让您能将更多宝贵的时间花在更有价值的任务上。

●  **xlwings创始人倾力打造，手把手教学** 

“将Python作为Excel的脚本语言”开源Python库xlwings的诞生很好地回答了这些问题，它让Excel和Python珠联璧合。而作为xlwings的创始人，本书作者将展示如何借用Python的力量，让Excel快得飞起来！

●  **更多设备，更简化的编程语言，一切只为更好的办公** 

使用Visual Studio Code和Jupyter笔记本等便捷工具，用Python替代VBA、Power Query和Power Pivot，支持更多设备，让您学习起来更加简单。